# Tenth class, 25 November 2024

Tree automata. Come in Büchi, Müller, Parity etc. flavour. ⚠ Büchi and Müller/Parity/etc. are not equivalent, even with non-deterministic automata.

S2S: theory with a two successor functions 0,1. Models elements and subsets of the binary rooted tree {0,1}^*.

Finkbeiner chapter 10.
